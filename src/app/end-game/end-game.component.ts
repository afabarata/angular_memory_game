import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-end-game',
  templateUrl: './end-game.component.html',
  styleUrls: ['./end-game.component.css']
})
export class EndGameComponent implements OnInit {
  timer: number;
  moves: number;

  constructor(
    private router: Router,
    private cookieService: CookieService,
  ) { }

  ngOnInit() {
    this.timer = Number(this.cookieService.get('UserTime'));
    this.moves = Number(this.cookieService.get('UserMoves'));
  }

  refreshGame() {
    this.router.navigate(['users']);
  }
}
