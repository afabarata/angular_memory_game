import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import {Piece} from '../piece';

import {PiecesService} from '../pieces.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  pieces: Piece[] = [];
  board: Piece[] = [];

  level: number;
  grid_class: string;
  num_single_pieces: number;

  first_flipped: Piece;
  stop_mouse: boolean;
  timer: number;
  moves: number;
  status: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cookieService: CookieService,
    private piecesService: PiecesService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
    this.level = params['level'];
    this.stop_mouse = false;
    switch (this.level.toString()) {
      case '1':
        this.grid_class = 'easy';
        this.num_single_pieces = 16 / 2 ;
        break;
      case '2':
        this.grid_class = 'medium';
        this.num_single_pieces = 20 / 2;
        break;
      case '3':
        this.grid_class = 'hard';
        this.num_single_pieces = 30 / 2;
        break;
      default:
        this.grid_class = '';
        this.num_single_pieces = 0;
    }
    });

    this.pieces = this.piecesService.getPieces();
    for (let _i = 0; _i < this.num_single_pieces; _i++) {
      const randomPiece = Math.floor(Math.random() * this.pieces.length - 1) + 1;
      this.board.push(Object.assign({}, this.pieces[randomPiece]));
      this.board.push(Object.assign({}, this.pieces[randomPiece]));
      this.shuffle(this.board);
    }

    this.startCounting();
    this.moves = 0;
    this.status = 0;
  }

  flip(piece: Piece) {
    if (piece.disabled === false && piece.flipped === false) {
      piece.flipped = true;
      if (this.first_flipped) {
        if (this.first_flipped.number === piece.number) {
          this.first_flipped.disabled = true;
          this.first_flipped = undefined;
          piece.disabled = true;
          this.status++;
          this.endGame();
        } else {
          this.stop_mouse = true;
          setTimeout(() => {
            this.first_flipped.flipped = false;
            this.first_flipped = undefined;
            piece.flipped = false;
            this.stop_mouse = false;
          }, 1000);
        }
        this.moves++;
      } else {
        this.first_flipped = piece;
      }
    }
  }

  private startCounting() {
    this.timer = 0;
    setInterval(() => {
      this.timer++;
    }, 1000);
  }

  private endGame() {
    if (this.isEnd()) {
      console.log('endGame');
      this.cookieService.set( 'UserMoves', this.moves.toString() );
      this.cookieService.set( 'UserTime', this.timer.toString() );
      this.router.navigate(['finish']);
    }
  }

  private isEnd(): boolean {
    if (this.status === this.num_single_pieces) {
      return true;
    }
    return false;
  }

  private shuffle(array) {
    let i = 0;
    let j = 0;
    let temp = null;

    for (i = array.length - 1; i > 0; i -= 1) {
      j = Math.floor(Math.random() * (i + 1));
      temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }
}
