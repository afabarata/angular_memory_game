import { Piece } from './piece';

export const PIECES: Piece[] = [
  { number: 1, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=1', flipped: false, disabled: false },
  { number: 2, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=2', flipped: false, disabled: false },
  { number: 3, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=3', flipped: false, disabled: false },
  { number: 4, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=4', flipped: false, disabled: false },
  { number: 5, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=5', flipped: false, disabled: false },
  { number: 6, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=6', flipped: false, disabled: false },
  { number: 7, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=7', flipped: false, disabled: false },
  { number: 8, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=8', flipped: false, disabled: false },
  { number: 9, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=9', flipped: false, disabled: false },
  { number: 10, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=10', flipped: false, disabled: false },
  { number: 11, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=11', flipped: false, disabled: false },
  { number: 12, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=12', flipped: false, disabled: false },
  { number: 13, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=13', flipped: false, disabled: false },
  { number: 14, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=14', flipped: false, disabled: false },
  { number: 15, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=15', flipped: false, disabled: false },
  { number: 16, image_top: 'https://fakeimg.pl/250x250/d8e6ad/?text=TOP', image_bottom: 'https://fakeimg.pl/250x250/ADD8E6/?text=16', flipped: false, disabled: false }
];
