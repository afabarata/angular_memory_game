import { User } from './user';

export const USERS: User[] = [
  { id: 11, name: 'Player 1', moves: 0, time: '' },
  { id: 12, name: 'Player 2', moves: 0, time: '' },
  { id: 13, name: 'Player 3', moves: 0, time: ''  }
];
