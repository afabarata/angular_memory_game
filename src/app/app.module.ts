import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { UserSelectionComponent } from './user-selection/user-selection.component';
import { GameComponent } from './game/game.component';
import { BoardComponent } from './board/board.component';
import { EndGameComponent } from './end-game/end-game.component';

import { UserDataService} from './user-data.service';
import { PiecesService} from './pieces.service';

import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    UserSelectionComponent,
    GameComponent,
    EndGameComponent,
    BoardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [
    CookieService,
    UserDataService,
    PiecesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
