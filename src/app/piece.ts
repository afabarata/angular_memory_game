export class Piece {
  number: number;
  image_top: string;
  image_bottom: string;
  flipped: boolean;
  disabled: boolean;
}
