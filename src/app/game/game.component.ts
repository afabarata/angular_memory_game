import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import {Level} from '../level';

export const LEVELS: Level[] = [
  { id: 0, name: '-- Choose a difficulty --' },
  { id: 1, name: 'Easy' },
  { id: 2, name: 'Medium' },
  { id: 3, name: 'Hard' },
];

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  userName: string;
  levels = Level[''];

  currentLevel: number;
  errorMessage: string;

  constructor(
    private cookieService: CookieService,
    private router: Router,
    private location: Location) { }

  ngOnInit(): void {
    this.userName = this.cookieService.get('UserName');
    this.levels = LEVELS;
    this.currentLevel = 0;
  }

  setNewLevel(id: any) {
    this.currentLevel = id;
  }

  startGame() {
    if (this.currentLevel !== 0) {
      this.router.navigate(['board', this.currentLevel]);
    } else {
      this.errorMessage = 'You have to select one level!';
    }
  }

  goBack(): void {
    this.location.back();
  }

}
