import { Injectable } from '@angular/core';

import { Piece } from './piece';
import { PIECES } from './mock-pieces';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class PiecesService {

  constructor() { }

  getPieces(): Piece[] {
    return PIECES;
  }

  /*getPiecesByAmount(amount: number): Promise<Piece[]> {
    return Promise.resolve(PIECES);
  }*/

}
