import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../user';
import { UserDataService } from '../user-data.service';

@Component({
  selector: 'app-user-selection',
  templateUrl: './user-selection.component.html',
  styleUrls: ['./user-selection.component.css']
})
export class UserSelectionComponent implements OnInit {
  title = 'Memory Game';
  users: User[] = [];

  constructor(private cookieService: CookieService, private userDataService: UserDataService) {}

  ngOnInit(): void {
    this.userDataService.getUsers().then(users => this.users = users);
  }

  userSelected(user: User) {
    this.cookieService.set( 'UserName', user.name );
  }
}
