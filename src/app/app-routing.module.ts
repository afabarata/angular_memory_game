import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserSelectionComponent } from './user-selection/user-selection.component';
import { GameComponent } from './game/game.component';
import { BoardComponent } from './board/board.component';
import { EndGameComponent } from './end-game/end-game.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/users',
    pathMatch: 'full'
  },
  {
    path: 'users',
    component: UserSelectionComponent
  },
  {
    path: 'game',
    component: GameComponent
  },
  {
    path: 'board/:level',
    component: BoardComponent
  },
  {
    path: 'finish',
    component: EndGameComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
