export class User {
  id: number;
  name: string;
  moves: number;
  time: string;
}
